package io.fintechlabs.testframework.condition.client;

import com.google.gson.JsonObject;
import io.fintechlabs.testframework.condition.AbstractCondition;
import io.fintechlabs.testframework.condition.PreEnvironment;
import io.fintechlabs.testframework.testmodule.Environment;

public class AddBadIssToRequestObject extends AbstractCondition {

	@Override
	@PreEnvironment(required = { "request_object_claims"})
	public Environment evaluate(Environment env) {
		JsonObject requestObjectClaims = env.getObject("request_object_claims");
		requestObjectClaims.addProperty("iss", "11111111111");
		env.putObject("request_object_claims", requestObjectClaims);

		logSuccess("Added bad iss to request object claims", args("iss", requestObjectClaims.getAsJsonPrimitive("iss")));
		return env;
	}
}
